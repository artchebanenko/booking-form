# Booking Form

Made w/ **redux-saga** and **react-final-form**  

Demo via GitLab Pages by CI  
https://artchebanenko.gitlab.io/booking-form-saga/

Demo via Netlify  
https://romantic-feynman-fe801c.netlify.app/

Same project w/ @reduxjs/toolkit and react-hook-form  
https://gitlab.com/artchebanenko/booking-form-toolkit